FROM node:latest
WORKDIR /alc-autobots-ui-red-theme
COPY ./package.json /alc-autobots-ui-red-theme
RUN npm install
EXPOSE 3000
COPY . .
CMD [ "npm", "run", "start" ]